package com.example.userlogin

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.userlogin.activities.HomeActivity
import com.example.userlogin.activities.LoginActivity
import com.example.userlogin.activities.SignUpActivity

class MainActivity : AppCompatActivity() {

    private var sharedPrefMode = 0
    private val sharedPrefName = "userData"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        val sharedPref: SharedPreferences = getSharedPreferences(sharedPrefName,sharedPrefMode)

        if (sharedPref.getBoolean("status", false)) {
            val redirect = Intent(this, LoginActivity::class.java)
            startActivity(redirect)
            this.finish()
        }
        else{
            val redirect = Intent(this, SignUpActivity::class.java)
            startActivity(redirect)
            this.finish()
        }
    }
}
