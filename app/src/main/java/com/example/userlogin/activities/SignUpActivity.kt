package com.example.userlogin.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.userlogin.R
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignUpActivity : AppCompatActivity() {

    private var sharedPrefMode = 0
    private val sharedPrefName = "userData"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        signUpBtn.setOnClickListener {

            if (signUpUsernameET.text.toString() != "") {
                val sharedPref: SharedPreferences = getSharedPreferences(sharedPrefName,sharedPrefMode)
                val edit = sharedPref.edit()
                edit.putString("userName",signUpUsernameET.text.toString())
                edit.putString("email",signUpEmailET.text.toString())
                edit.putString("password",signUpPasswordET.text.toString())
                edit.putBoolean("status",true)
                edit.apply()

                val toast = Toast.makeText(applicationContext, "Successfully Registered...", Toast.LENGTH_LONG)
                toast.show()

                val login = Intent(this, LoginActivity::class.java)
                startActivity(login)
                this.finish()
            }
            else{
                val toast = Toast.makeText(applicationContext, "Username is Empty...", Toast.LENGTH_LONG)
                toast.show()
            }
        }



    }
}
