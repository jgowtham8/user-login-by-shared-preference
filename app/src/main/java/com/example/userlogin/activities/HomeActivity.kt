package com.example.userlogin.activities

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.userlogin.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val username: String? = intent.getStringExtra("username")
        val password: String? = intent.getStringExtra("password")

        tvUser.text = username
        tvPass.text = password

        clear.setOnClickListener {
            val sharedPref: SharedPreferences = getSharedPreferences("userData",0)
            sharedPref.edit().clear().apply()

            val toast = Toast.makeText(applicationContext, "Shared Pref cleared...", Toast.LENGTH_LONG)
            toast.show()
        }
    }
}
