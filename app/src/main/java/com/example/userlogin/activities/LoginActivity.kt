package com.example.userlogin.activities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.Toast
import com.example.userlogin.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private var sharedPrefMode = 0
    private val sharedPrefName = "userData"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginBtn.setOnClickListener {
            val sharedPref: SharedPreferences = getSharedPreferences(sharedPrefName,sharedPrefMode)
            val tempUserName = sharedPref.getString("userName",null)
            val tempPassword = sharedPref.getString("password",null)

            if ((tempUserName == loginUsernameET.text.toString()) && (tempPassword == loginPasswordET.text.toString())){
                val home = Intent(this, HomeActivity::class.java)
                home.putExtra("username", tempUserName)
                home.putExtra("password", tempPassword)
                startActivity(home)
            }
            else{
                val toast = Toast.makeText(applicationContext, "Invalid Login", Toast.LENGTH_LONG)
                //toast.setGravity(Gravity.BOTTOM,0,0)
                toast.show()
            }
        }

        loginSignUpBtn.setOnClickListener {
            val signUp = Intent(this, SignUpActivity::class.java)
            startActivity(signUp)
        }
    }
}
